package com.crea.dev4.first.gregmendez.demo.controller;


import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.crea.dev4.first.gregmendez.demo.dao.FavorisDao;
import com.crea.dev4.first.gregmendez.demo.model.Favoris;



@RestController
public class FavorisController {

    private final FavorisDao repository;

    public FavorisController(FavorisDao repository) {
        this.repository = repository;
    }

    @GetMapping("/favoris")
    public List<Favoris> displayFavoris() {
        return repository.findAll();
    }

    @GetMapping("/favoris/{idUser}")
    public List<Favoris> displayFavorisByIdUser(@PathVariable int idUser) {
        return repository.findByIdUser(idUser);
    }

    @PostMapping("/favoris/add")
    public void addFavoris(int idUser, int idProduct) {
        Favoris newFavoris = new Favoris();

        newFavoris.setIdUser(idUser);
        newFavoris.setIdProduct(idProduct);

        repository.save(newFavoris);
    }

    @DeleteMapping("/favoris/{id}")
    public void deleteFavoris(@PathVariable int id) {
        repository.deleteById(id);
    }
}
