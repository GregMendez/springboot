package com.crea.dev4.first.gregmendez.demo.controller;


import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crea.dev4.first.gregmendez.demo.dao.ClientDao;
import com.crea.dev4.first.gregmendez.demo.model.Client;


@RestController
public class ClientController {

    private final ClientDao repository;

    public ClientController(ClientDao repository) {
        this.repository = repository;
    }

    @GetMapping("/clients")
    public List<Client> displayClients() {
        return repository.findAll();
    }

    @GetMapping("/client/{id}")
    public Client displayClient(@PathVariable int id) {
        return repository.findById(id);
    }

    @PostMapping("/client/add")
    public void addClient(String email, String password, String validationPassword, String ip) {
        Client newClient = new Client();

        newClient.setEmail(email);
        
        if(password.equals(validationPassword)){
            newClient.setPassword(validationPassword);
        }
        
        newClient.setIp(ip);

        repository.save(newClient);
    }

    @DeleteMapping("/client/{id}")
    public void deleteClient(@PathVariable int id) {
        repository.deleteById(id);
    }
}
