package com.crea.dev4.first.gregmendez.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crea.dev4.first.gregmendez.demo.model.Product;


@Repository
public interface ProductDao extends JpaRepository<Product, Integer> {


    public List<Product> findAll();
    
    public Product findById(int id);

    public List<Product> findByPriceGreaterThan(int price);

    public Product findByName(String name);
    
}
