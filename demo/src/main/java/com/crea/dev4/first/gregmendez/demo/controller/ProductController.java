package com.crea.dev4.first.gregmendez.demo.controller;


import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crea.dev4.first.gregmendez.demo.dao.ProductDao;
import com.crea.dev4.first.gregmendez.demo.model.Product;



@RestController
public class ProductController {

    private final ProductDao repository;

    public ProductController(ProductDao repository) {
        this.repository = repository;
    }

    @GetMapping("/products")
    public List<Product> displayProducts() {
        return repository.findAll();
    }

    @GetMapping("/product/{id}")
    public Product displayProduct(@PathVariable int id) {
        return repository.findById(id);
    }

    @PostMapping("/product/add")
    public void addProduct(String category, String name, float price) {
        Product newProduct = new Product();

        newProduct.setCategory(category);
        newProduct.setName(name);
        newProduct.setPrice(price);

        repository.save(newProduct);
    }

    @DeleteMapping("/product/{id}")
    public void deleteProduct(@PathVariable int id) {
        repository.deleteById(id);
    }
}
