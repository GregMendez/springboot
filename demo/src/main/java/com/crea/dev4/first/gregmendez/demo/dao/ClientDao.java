package com.crea.dev4.first.gregmendez.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crea.dev4.first.gregmendez.demo.model.Client;


@Repository
public interface ClientDao extends JpaRepository<Client, Integer> {


    public List<Client> findAll();
    
    public Client findById(int id);
        
}
