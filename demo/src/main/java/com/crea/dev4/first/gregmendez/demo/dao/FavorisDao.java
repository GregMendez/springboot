package com.crea.dev4.first.gregmendez.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crea.dev4.first.gregmendez.demo.model.Favoris;


@Repository
public interface FavorisDao extends JpaRepository<Favoris, Integer> {


    public List<Favoris> findAll();
    
    public List<Favoris> findByIdUser(int idUser);

        
}
